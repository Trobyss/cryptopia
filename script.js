let system = document.querySelector('#system')
let planet = {
    option: {
        ring: 4,
    },
    planets:[
        {
            name:"Bitcoin"
        },
        {
            name:"Bitcoin"
        },
        {
            name:"Bitcoin"
        },
        {
            name:"Bitcoin"
        },
        {
            name:"Bitcoin"
        },
        {
            name:"Bitcoin"
        },
        {
            name:"Bitcoin"
        },
        {
            name:"Bitcoin"
        },
        {
            name:"Bitcoin"
        },
        {
            name:"Bitcoin"
        },
        {
            name:"Bitcoin"
        },
        {
            name:"Bitcoin"
        },
        {
            name:"Bitcoin"
        },
        {
            name:"Bitcoin"
        }
    ]
}
function init(){
    let solarsytem = document.createElement('section')
    solarsytem.id = "solar-system"
    system.appendChild(solarsytem)
    create_ring(solarsytem)
    create_planet(solarsytem)
}
function create_ring(solarsytem){
    for (let index = 1; index <= planet.option.ring; index++) {
        let div = document.createElement('div')
        div.classList.add("ring", "ring-" + index)
        solarsytem.appendChild(div)
    }
    let center = document.createElement('div')
    center.classList.add("center")
    solarsytem.appendChild(center)
}
function create_planet(solarsytem){
    planet.planets.map((planet, key) =>{
        let div = document.createElement('div')
        div.classList.add("planet", "planet-orbit-" + (key+1))
        div.id = planet.name
        solarsytem.appendChild(div)
    }) 
}
init()

function step_one(){
    let selection = document.querySelector('.wrap')
    selection.classList.add('move_title')
    let center = document.querySelector('.center')
    center.classList.add('override')
    let menu = document.querySelector('#menu')
    menu.classList.add('appear')
}